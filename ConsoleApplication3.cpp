#include <iostream> 
#include <iomanip> 
#include <math.h>
using namespace std;
int main()
{
	double x, y, x1, x2, h;
	cout << "Enter x1\n";
	cin >> x1;
	cout << "Enter x2\n";
	cin >> x2;
	cout << "Enter h\n";
	cin >> h;
	x = x1;
	do {
		y = pow(x, 3) + log(x) - 3;
		cout << setw(10) << "x = " << x << setw(10) << "y = " << y << endl;
		x += h;
	} while (x <= x2);
	cout << endl;
	x = x1;
	while (x <= x2) {
		y = pow(x, 3) + log(x) - 3;
		cout << setw(10) << "x = " << x << setw(10) << "y = " << y << endl;
		x += h;
	}
}
